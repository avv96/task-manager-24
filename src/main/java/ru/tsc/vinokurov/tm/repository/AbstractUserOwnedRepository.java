package ru.tsc.vinokurov.tm.repository;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tsc.vinokurov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.vinokurov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {
    @NonNull
    @Override
    public List<M> findAll(@NonNull final String userId) {
        return items.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @NonNull
    @Override
    public List<M> findAll(@NonNull final String userId, @NonNull final Comparator comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M add(@NonNull final String userId, @NotNull @NonNull final M item) {
        item.setUserId(userId);
        return add(item);
    }

    @Nullable
    @Override
    public M remove(@NonNull final String userId, @NotNull @NonNull final M item) {
        if (userId != item.getUserId()) return null;
        return remove(item);
    }

    @Override
    public void clear(@NonNull final String userId) {
        final List<M> items = findAll(userId);
        removeAll(items);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NonNull final String userId, @NonNull final Integer index) {
        return findAll(userId).get(index);
    }

    @Nullable
    @Override
    public M findOneById(@NonNull final String userId, @NonNull final String id) {
        return items.stream()
                .filter(item -> userId.equals(item.getUserId()) && id.equals(item.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public int size(@NonNull final String userId) {
        return findAll(userId).size();
    }

}
