package ru.tsc.vinokurov.tm.exception.field;

public final class CollectionEmptyException extends AbstractFieldException {

    public CollectionEmptyException() {
        super("Error! Collection is empty...");
    }

}
