package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {
    @NonNull
    Project create(@Nullable String userId, @Nullable String name);

    @NonNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NonNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NonNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NonNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NonNull
    Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NonNull
    Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
