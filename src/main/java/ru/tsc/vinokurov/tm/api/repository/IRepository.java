package ru.tsc.vinokurov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {
    @NonNull
    List<M> findAll();

    @NonNull
    List<M> findAll(Comparator comparator);

    @NonNull
    M add(@NonNull M item);

    @NonNull
    M remove(@NonNull M item);

    void clear();

    @Nullable
    M findOneByIndex(@NonNull Integer index);

    @Nullable
    M findOneById(@NonNull String id);

    @Nullable
    M removeById(@NonNull String id);

    @Nullable
    M removeByIndex(@NonNull Integer index);

    void removeAll(@NonNull Collection<M> collection);

    int size();

}
