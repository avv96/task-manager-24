package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {
    @NonNull
    Collection<AbstractCommand> getTerminalCommands();

    @NonNull
    Collection<AbstractCommand> getArgumentCommands();

    @NonNull
    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

}
