package ru.tsc.vinokurov.tm.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {
    @NonNull
    Task create(@Nullable String userId, @Nullable String name);

    @NonNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NonNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NonNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NonNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NonNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NonNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
