package ru.tsc.vinokurov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {
    @NonNull
    @Override
    M removeById(@Nullable String id);

    @NonNull
    @Override
    M removeByIndex(@Nullable Integer index);

    @Nullable
    List<M> findAll(@Nullable Sort sort);

}
