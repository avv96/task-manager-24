package ru.tsc.vinokurov.tm.api.model;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Status;

public interface IHasStatus {
    @NonNull
    Status getStatus();

    void setStatus(@NonNull Status status);

}
