package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-lock";

    @NonNull
    public static final String DESCRIPTION = "Lock user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NonNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
