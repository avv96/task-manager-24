package ru.tsc.vinokurov.tm.command.task;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-start-by-index";

    @NonNull
    public static final String DESCRIPTION = "Start task by index.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NonNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NonNull final String userId = getUserId();
        getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
