package ru.tsc.vinokurov.tm.command.project;

import lombok.NonNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NonNull
    public static final String NAME = "project-clear";

    @NonNull
    public static final String DESCRIPTION = "Clear project list.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        @NonNull final String userId = getUserId();
        getProjectService().clear(userId);
    }

}
