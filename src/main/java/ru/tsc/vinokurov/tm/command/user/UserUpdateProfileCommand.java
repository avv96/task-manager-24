package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "user-update-profile";

    @NonNull
    public static final String DESCRIPTION = "Update user profile.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NonNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NonNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NonNull final String middleName = TerminalUtil.nextLine();
        @NonNull final String userId = getAuthService().getUserId();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
