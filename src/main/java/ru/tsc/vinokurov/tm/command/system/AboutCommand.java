package ru.tsc.vinokurov.tm.command.system;

import lombok.NonNull;

public final class AboutCommand extends AbstractSystemCommand {

    @NonNull
    public static final String NAME = "about";

    @NonNull
    public static final String DESCRIPTION = "Show developer info.";

    @NonNull
    public static final String ARGUMENT = "-a";

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NonNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikolay Bubyakin");
        System.out.println("E-mail: nbubyakin@t1-consulting.ru");
    }

}
