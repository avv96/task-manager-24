package ru.tsc.vinokurov.tm.command.system;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.api.service.ICommandService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;
import ru.tsc.vinokurov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NonNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
