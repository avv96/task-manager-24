package ru.tsc.vinokurov.tm.command.user;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NonNull
    public static final String NAME = "login";

    @NonNull
    public static final String DESCRIPTION = "Login user.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NonNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NonNull final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
