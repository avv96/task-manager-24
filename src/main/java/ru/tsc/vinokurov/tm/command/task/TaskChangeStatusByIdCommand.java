package ru.tsc.vinokurov.tm.command.task;

import lombok.NonNull;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NonNull
    public static final String NAME = "task-change-status-by-id";

    @NonNull
    public static final String DESCRIPTION = "Change task status by id.";

    public static final String ARGUMENT = null;

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NonNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.asList(Status.values()));
        @NonNull final String statusValue = TerminalUtil.nextLine();
        @NonNull final Status status = Status.toStatus(statusValue);
        @NonNull final String userId = getUserId();
        getTaskService().changeStatusById(userId, id, status);
    }

}
