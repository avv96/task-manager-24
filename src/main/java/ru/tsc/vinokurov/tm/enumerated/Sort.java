package ru.tsc.vinokurov.tm.enumerated;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.vinokurov.tm.comparator.CreatedComparator;
import ru.tsc.vinokurov.tm.comparator.DateBeginComparator;
import ru.tsc.vinokurov.tm.comparator.NameComparator;
import ru.tsc.vinokurov.tm.comparator.StatusComparator;

import java.util.Arrays;
import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);
    @NonNull
    private final Comparator comparator;
    @NonNull
    @Setter
    private String displayName;

    Sort(@NonNull String displayName, @NonNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        return Arrays.stream(values())
                .filter(sort -> sort.name().equals(value))
                .findAny().orElse(null);
    }

}
