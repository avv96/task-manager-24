package ru.tsc.vinokurov.tm.enumerated;


import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.exception.field.StatusEmptyException;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");
    @NonNull
    private String displayName;

    Status(@NonNull String displayName) {
        this.displayName = displayName;
    }

    @NonNull
    public static String toName(@Nullable Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (StringUtils.isEmpty(value)) throw new StatusEmptyException();
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

}
