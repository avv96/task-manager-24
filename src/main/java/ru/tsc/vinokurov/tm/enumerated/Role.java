package ru.tsc.vinokurov.tm.enumerated;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.exception.field.StatusEmptyException;

import java.util.Arrays;
@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");
    @NonNull
    private final String displayName;

    Role(@NonNull final String displayName) {
        this.displayName = displayName;
    }

    @NonNull
    public static Role toRole(@Nullable final String value) {
        if (StringUtils.isEmpty(value)) return null;
        return Arrays.stream(values()).filter(role -> role.name().equals(value)).findAny().orElseThrow(StatusEmptyException::new);
    }

    @Nullable
    public static String toName(@Nullable final Role role) {
        if (role == null) return "";
        return role.getDisplayName();
    }

}
