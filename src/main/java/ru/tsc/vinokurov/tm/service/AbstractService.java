package ru.tsc.vinokurov.tm.service;


import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.api.service.IService;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.exception.entity.ItemNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.CollectionEmptyException;
import ru.tsc.vinokurov.tm.exception.field.IdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.IndexIncorrectException;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {
    @NonNull
    protected final R repository;

    public AbstractService(@NonNull final R repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M add(@Nullable final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.add(item);
    }

    @Nullable
    @Override
    public M remove(@Nullable final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.remove(item);
    }

    @NonNull
    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) throw new CollectionEmptyException();
        repository.removeAll(collection);
    }

    @NonNull
    @Override
    public M removeById(@Nullable final String id) {
        final M item = Optional.ofNullable(findOneById(id)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(item);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        final M item = Optional.ofNullable(findOneByIndex(index)).orElseThrow(ItemNotFoundException::new);
        return repository.remove(item);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= repository.size()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public int size() {
        return repository.size();
    }

}
